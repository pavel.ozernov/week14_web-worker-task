const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com'];

//================== validate ======================

export function validate(email) {
   // Use a regular expression to match the email address pattern
   const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
   if (!emailRegex.test(email)) {
     return false;
   }
 
   // Check if the domain of the email address is in the list of valid domains
   const domain = email.split('@')[1];
   return VALID_EMAIL_ENDINGS.includes(domain);
 };

//==================== validateAsync ====================

 export function validateAsync(email) {
  return new Promise((resolve, reject) => {
    // Check if email ends with ".com"
    if (email.endsWith(".com")) {
      // Email is valid
      resolve(true);
    } else {
      // Email is invalid
      resolve(false);
    }
  });
};

//==================== validateWithThrow ====================

export function validateWithThrow(email) {
  if (email.endsWith('.com')) {
    return true;
  } else {
    throw new Error('The provided email is invalid.');
  }
};

//================== validateWithLog based on validate =====================

export function validateWithLog(email) {
  const isValid = validate(email);
  console.log(`Email validation result for "${email}": ${isValid}`);
  return isValid;
}


