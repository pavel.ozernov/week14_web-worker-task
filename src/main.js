import onload from './join-us-section.js';
import "./styles/style.css";
import "./styles/normalize.css";
import {communitySection} from './community';

const worker = new Worker(new URL('../src/web_worker.js', import.meta.url));
//const worker = new Worker('./web_worker.js'); 

const requestURLcommunity = 'http://localhost:3000/community';


//standard or advanced
window.onload = () => {
    onload.create('standard');
    communitySection(requestURLcommunity);
    
};

//================== web-worker ================

console.log('webworker is ON 01')

const buttons = document.querySelectorAll('button');
const emailInput = document.querySelector('input');
if (emailInput){
  console.log('INPUT')
} else{
  console.log('No INPUT')
}

buttons.forEach(button => {
  button.addEventListener('click', e => {
    const eventData = {
      type: 'button-click',
      target: e.target.innerText,
      timestamp: Date.now()
    };
    console.log("the button data" + eventData["timestamp"]);
   worker.postMessage(eventData);
  });
});

// emailInput.addEventListener('input', e => {
//   const eventData = {
//     type: 'email-input',
//     value: e.target.value,
//     timestamp: Date.now()
//   };
//   worker.postMessage(eventData);
// });
