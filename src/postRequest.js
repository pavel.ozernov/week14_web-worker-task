//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
export function postRequest (method, url, body = null) {
    return new Promise((resolve, reject) => {
     const xhr = new XMLHttpRequest();
    
     xhr.open(method, url);
     xhr.setRequestHeader ('Content-Type', 'application/json');       
     xhr.responseType = 'json';
     
     xhr.onload = () => {
       if (xhr.status >= 400) {
         reject(xhr.response)
       } else {
        resolve(xhr.response)
     }
   }
     xhr.onerror = () => {
       console.log(xhr.response)
     }
     xhr.send(JSON.stringify(body))
    
})

};





