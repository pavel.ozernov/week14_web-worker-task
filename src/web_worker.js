const batchSize = 5;
let events = [];

onmessage = function(event) {
  events.push(event.data);
  console.log("onmessage data" + event.data)
  if (events.length >= batchSize) {
    sendBatch(events);
    events = [];
  }
};

function sendBatch(events) {
  console.log('-----posting-------')
  const xhr = new XMLHttpRequest();
  xhr.open('POST', 'http://localhost:3000/analytics/user', true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.send(JSON.stringify(events));
}

