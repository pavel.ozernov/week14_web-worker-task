const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require("copy-webpack-plugin");
const WorkerPlugin = require('worker-plugin');


module.exports = {
    mode: 'development',
    entry: {
      main: path.resolve(__dirname, 'src/main.js')
    },
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: '[name].[contenthash].js',

      clean: true,
    },
    devtool: 'inline-source-map',
    devServer:{
      static: path.resolve(__dirname, 'dist'),
      port:8080,
      open:true,
      hot: true,
    },

//loaders

    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env']
            }
          }
        },
        {
          test: /\.css$/i,
          use: ["style-loader", "css-loader"],
        },
        {
          test: /\.(png|svg|jpg|jpeg|gif)$/i,
          type: 'asset/resource',
        },

      ],
    },


//plugins
plugins: [
  new HtmlWebpackPlugin({
    template: 'src/index.html'
  }),
  new CopyPlugin({
  patterns: [
    { from: 'src/assets/images/your-logo-footer.png', to: './assets/images/your-logo-footer.png' },
    { from: 'src/assets/images/your-logo-here.png', to: './assets/images/your-logo-here.png' },
    { from: 'src/assets/images/community_placeholder_01.png', to: './assets/images/community_placeholder_01.png'},
    { from: 'src/assets/images/community_placeholder_02.png', to: './assets/images/community_placeholder_02.png'},
    { from: 'src/assets/images/community_placeholder_03.png', to: './assets/images/community_placeholder_03.png'},
    ],
  }),

  new WorkerPlugin({
    // use "workerPathFilename" option to specify the name of the worker file
    // this is optional and defaults to "[name].worker.js"
    workerPathFilename: 'src/web_worker.js',
  }),

],
  };